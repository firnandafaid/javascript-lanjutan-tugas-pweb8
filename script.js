function validasiForm() {
    const name =
        document.getElementById("name").value;
    const username =
        document.getElementById("username").value;
    const email =
        document.getElementById("email").value;
    const password =
        document.getElementById("password").value;
    const subject =
        document.getElementById("subject").value;

    const nameError =
        document.getElementById("name-error");
    const usernameError = document.getElementById(
        "username-error"
    );
    const emailError = document.getElementById(
        "email-error"
    );
    const passwordError = document.getElementById(
        "password-error"
    );
    const subjectError = document.getElementById(
        "subject-error"
    );

    nameError.textContent = "";
    usernameError.textContent = "";
    emailError.textContent = "";
    passwordError.textContent = "";
    subjectError.textContent = "";

    let isValid = true;

    if (name === "" || /\d/.test(name)) {
        nameError.textContent =
            "Please, Enter your name correctly!";
        isValid = false;
    }

    if (username === "") {
        usernameError.textContent =
            "Please, Enter your Username correctly!";
        isValid = false;
    }

    if (email === "" || !email.includes("@")) {
        emailError.textContent =
            "Enter your valid email here!";
        isValid = false;
    }

    if (password === "" || password.length < 6) {
        passwordError.textContent =
            "Password must be at least 6 characters!";
        isValid = false;
    }

    if (subject === "") {
        subjectError.textContent =
            "Must be filled!";
        isValid = false;
    }
    
    if (isValid) {
        alert("Thanks!Your Form has been submitted!");
    }
    
    return isValid;
}
